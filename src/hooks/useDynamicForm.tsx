import * as Yup from 'yup';

type dynamicInitialValueSchemaProps = {
  name: string;
  type: string;
  label: number;
  placeholder: string;
  required:boolean;
  validations: {
    type: any;
    options: [any, string];
  }[];
}[];




const sanitizeYupType = (type: string) => {
  switch (type) {
    case 'max-length':
      return 'max';
    case 'min-length':
      return 'min';
    default:
      return type;
  }
};

const optionValidation = (option: [any, string]): Array<any> => {
  if (typeof option[0] === 'boolean') {
    return [option[1]];
  } else {
    return option;
  }
};

export const createDynamicInitialValues = (formElements?: dynamicInitialValueSchemaProps) => {
  const initialValue: any = {};
  formElements?.forEach((element) => {
    initialValue[element.name] = {
      label: element.label,
      value: ''
    };
  });

  return initialValue;
};

export const dynamicValidationSchema = (formElements?: dynamicInitialValueSchemaProps) => {
  const initialValue: any = {};

  formElements?.forEach((formElement) => {
    let value = (Yup as any)[formElement.dataType === 'object' ? 'mixed' : formElement.dataType]();

    formElement.validationDetail.forEach((validation) => {
      if (validation.options[0] !== false) {
        value = value[sanitizeYupType(validation.type)](...optionValidation(validation.options));
      }
    });
    initialValue[formElement.fieldName] = Yup.object({
      label: Yup.string(),
      value
    });
  });

  const declaredData = (
    name: string,
    validationForInitialValue: {
      [key: string]: any | undefined;
    }
  ) => {
    return Yup.object().shape({
      ...validationForInitialValue,
      [name]: Yup.object({ ...initialValue })
    });
  };

  return { declaredData };
};

const useDynamicForm = (
  // args: any | undefined | [],
  formData: any,
  // id: number | undefined | null,
  name: string,
  dynamicFormValue?: any
) => {
  // const data = args?.filter((arg: any) => {
  //   return arg.id === (formData?.item?.value || id);
  // })?.[0]?.specifications;

  // console.log(args, id);

  // console.log(adminStockItem, dynamicFormValue, 'adminStockForm');

  const { declaredData } = dynamicValidationSchema(dynamicFormValue?.specifications || []);

  const InitialValue = {
    ...formData,
    [name]: { ...createDynamicInitialValues(dynamicFormValue?.specifications || []) }
  };

  return { InitialValue, declaredData };
};

export default useDynamicForm;