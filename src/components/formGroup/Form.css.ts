import { Field } from "formik";
import styled from "styled-components"
export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  font-size:18px ;
  font-weight:400 ;
  
`;

export const FlexField = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap ;
  font-size:18px ;
  font-weight:400 ;
  align-items: center ;
  gap: 10px;
`;


export const Error = styled.div`
  color: red;
  font-size: 0.8rem;
  margin-top:5px ;
`;

export const InputField = styled(Field)`
  width: 100%;
  height: 50px;
  padding: 0 10px;
  font-family: "Montserrat";
  border-radius:0px ;
  border:1px solid black ;
  margin:10px 0;

`;


export const InputLabel = styled.label`
  width: 100%;
  font-family: "Montserrat";
  margin-bottom:10px ;
  text-align: left;
`;

export const TextField = styled(Field)`
  width: 100%;
  height: 200px;
  padding: 10px;
  font-family: "Montserrat";
  border-radius:0px ;
  border:1px solid black ;
`;

export const FormContainer = styled.div`
    width: 500px;
    margin: 50px auto;
    

`


export const UploadSectionContainer = styled.div`
  height:100% ;
  border: 1px dotted black ;
`

export const UploadSectionRoot = styled.div`
  padding:10px ;
`

export const UploadSectionInput = styled.div`

`

export const UploadMessage = styled.p`
  font-size:14px ;

`



export const RadioInput = styled.div`
  display: flex ;
  gap:5px ;
  align-items: center ;
  
`

export const RadioLabel = styled.label`
  font-size:16px ;

  
`