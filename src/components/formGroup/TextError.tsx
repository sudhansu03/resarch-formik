import React from "react";
import { Error } from "./Form.css";

const TextError = (props:any) => {
  return <Error>{props.children}</Error>;
};

export default TextError;
