import { ErrorMessage } from "formik";
import { useDropzone } from "react-dropzone";
import TextError from "./TextError";
import {
  InputContainer,
  InputLabel,
  UploadMessage,
  UploadSectionContainer,
  UploadSectionInput,
  UploadSectionRoot,
} from "./Form.css";

const UploadComponent = (props:any) => {
  const { label, name, setFieldValue, value, acceptedFiles} =
    props;

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
      accept: acceptedFiles ? acceptedFiles : "",
      multiple: false,
      onDrop: (acceptedFiles) => {
        setFieldValue(`${name}`, acceptedFiles);
      },
    });
    return (
      <UploadSectionContainer>
        <UploadSectionRoot {...getRootProps()}>
          <UploadSectionInput {...getInputProps()} />
          {value ? (
            value.map((file:any, i:any) => (
              <UploadMessage key={i}>
                {`File: ${file.name}`}
                <br />
                {`Type: ${file.type}    Size: ${file.size} bytes`}
              </UploadMessage>
            ))
          ) : (
            <>
              {isDragActive ? (
                <UploadMessage>Drop the file here ...</UploadMessage>
              ) : (
                <UploadMessage>
                  Drag & drop your file here, or click to select file
                </UploadMessage>
              )}
              {acceptedFiles && (
                <UploadMessage>
                  {label.acceptedFiles
                    ? `${label.acceptedFiles} : ${acceptedFiles}`
                    : `AcceptedFiles: ${acceptedFiles}`}
                </UploadMessage>
              )}
            </>
          )}
        </UploadSectionRoot>
      </UploadSectionContainer>
    );
  };

export default UploadComponent;
