import React from "react";
import UploadComponent from "./UploadComponent";
import { InputContainer, InputField, InputLabel,TextField , RadioInput,
  FlexField,
  RadioLabel} from "./Form.css";
import { ErrorMessage } from "formik";
import TextError from "./TextError";



interface Props {
  boolean?: boolean;
  disabled?: any;
  type: any;
  label?: any;
  name: any;
  value?: any;
  icon?: any;
  formikHandleBlur?: any;
  formikHandleChange?: any;
  onChangeCallbackAction?: any;
  errors?: any;
  touched?: any;
  setFieldValue?: any;
  setFieldTouched?: any;
  notRequired?: any;
  radioOptions?: {label:string, value:string}[];
  checkboxOptions?: {label:string, value:string}[];
  selectSettings?: any;
  dateSettings?: any;
  t?: any;
  getNameSpace?: (i18nKey: string) => string;
  style?: any;
  handleBlur?: any;
  placeholder?: string;
  rows?: number;
  iconClick?: any;
  commaSeparated?: boolean;
  isNepali?: boolean;
  setEnglishDateFromNepaliDatePicker?: (value: any) => void;
  translateLabel?: boolean;
  maxDate?: any;
  minDate?: any;
  onInputChange?: (value: string) => void;
  autoComplete?:boolean;
  acceptedFiles?:string;
  selectOptions?:{label:string, value:string}[];
}

const FormikControl = (props:Props) => {
  const {
    disabled,
    type,
    label,
    name,
    value,
    icon,
    formikHandleBlur,
    formikHandleChange,
    onChangeCallbackAction,
    errors,
    touched,
    setFieldValue,
    setFieldTouched,
    notRequired,
    radioOptions,
    selectSettings,
    dateSettings,
    t,
    getNameSpace,
    translateLabel,
    rows,
    placeholder,
    checkboxOptions,
    iconClick,
    isNepali,
    commaSeparated,
    setEnglishDateFromNepaliDatePicker,
    maxDate,
    minDate,
    onInputChange,
    autoComplete,
    acceptedFiles,
    selectOptions
  } = props;

  const renderField = () => { 
      switch (type) {
        case "text":
        case "email":
        case "password":
        case "number":
        case "time":
          return <InputField placeholder={placeholder}  value={value} id={name} name={name} type={type} autoComplete={autoComplete? "on":"off" } readOnly={disabled}/>;
        case "textarea":
          return <InputField component="textarea" id={name} name={name} rows={rows || 7} value={value}
          disabled={disabled} placeholder={placeholder}  onChange={(e: any) => {
            formikHandleChange(e);
            if (onChangeCallbackAction) {
              onChangeCallbackAction(e);
            }
          }}  />;

        case "radio":
          return <FlexField>
          <InputField name={name} >
            {({ field }:{field:any}) => {
              return radioOptions?.map((option:any) => {
                return (
                  <RadioInput key={option.label}>
                    <input
                      type="radio"
                      id={`${name}-${option.value}`}
                      {...field}
                      value={option.value}
                      checked={field.value === option.value}
                    />
                    <RadioLabel htmlFor={option.value}>
                      {option.label}
                    </RadioLabel>
                  </RadioInput>
                );
              });
            }}
          </InputField>
        </FlexField>;

        case "checkbox":
          return <FlexField>
          <InputField name={name} >
            {({ field }:{field:any}) => {
              return checkboxOptions?.map((option:any) => {
                return (
                  <RadioInput key={option.label}>
                    <input
                      type="checkbox"
                      id={`${name}-${option.value}`}
                      {...field}
                      value={option.value}
                      checked={field?.value?.includes(option.value)}
                    />
                    <RadioLabel htmlFor={option.value}>
                      {option.label}
                    </RadioLabel>
                  </RadioInput>
                );
              });
            }}
          </InputField>
        </FlexField>;

        case "file":
          return <input
          type="file"
          aria-label="i"
          name={name}
          onChange={(event) => {
            setFieldValue(`${name}`,event?.currentTarget?.files && event?.currentTarget?.files[0]);
          }}
        />;

        case "select":
          return <select
          name={name}
          value={value}
        >
          <option value="" label={placeholder}>
            {placeholder}
          </option>
          {selectOptions?.map((option:any) => {
              return (
                <option value={option.value} label={option.label} key={option.label}>
                {option.label}
                </option>
              );
            })
          }
        </select>

        case "date":
          return <InputField type={type} id="start" name={name}
          value={value}
          min={minDate} max={maxDate}/>

        
        case "drag":
          return  <UploadComponent setFieldValue={setFieldValue} label={label} name={name} value={value} acceptedFiles={acceptedFiles} />;
       
        default:
          return null;
      }
}
return (
    <InputContainer>
      <InputLabel htmlFor={name}>{label}</InputLabel>
      {renderField()}
      <ErrorMessage name={name} component={TextError} />
    </InputContainer>
);
};

export default FormikControl;
