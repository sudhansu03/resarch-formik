export enum regex {
    MOBILENUMBER = "[0-9]{10}",
    UNIVERSALNUMBER =" ^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$"    ,
    //landline
    PHONENUMBER = "[0-9]{7}",

    NUMBERONLY = "/\d+/g",

    LETTERONLY = "/^[a-zA-Z\s]*$/",

    EMAIL = "/^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6})*$/",

    // 8 characters which contain at least one numeric digit, one uppercase and one lowercase letter and a special character
    PASSWORD= "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})",

    ALPHANUMERIC = "/^[a-zA-Z0-9 ]*$/",

    // 12/25/2009
    SHORTDATE = "d?d/d?d/dddd",
  
    // 17:50
    TIME = "[0-9]?[0-9]:[0-9][0-9]",
  }
  