import { Formik } from 'formik';
import React from 'react'
import FormikControl from '../components/formGroup/FormikControl';
const GenericForm = () => {
const formSchema = [
  {
    name:"fullName",
    type:"text",
    label:"Full Name",
    placeholder:"Enter Full Name",
    validations:{
      required: true,
      message:"Please Enter Full Name",
      type:"string", 
    }
  },
  {
    name:"email",
    type:"email",
    label:"Email Name",
    placeholder:"Enter Email Address",
    validations:{
      required: true,
      message:"Please Enter Email",
      type:"email", 
    }
  },
  {
    name:"address",
    type:"text",
    label:"Address",
    placeholder:"Enter Address",
    required:true,
    dataType:"string"
    validations:
    [

    {
      type:;
      options: ;
    }
  ]
  }
];

  return (
    <Formik
      enableReinitialize={true}
      initialValues={createDynamicInitialValues(formSchema)}
      validationSchema={}
      onSubmit={(values, { resetForm }) => {
        
      }}
    >
      {({
        values,
        touched,
        errors,
        handleBlur,
        handleChange,
        setFieldTouched,
        setFieldValue,
        resetForm,
      }) => {
        
        return (
          <>
          {formSchema?.map((schema:any, index:number)=>{

            return (
              <FormikControl type={schema.type} name={schema.name} placeholder={schema.placeholder} label={schema.label} />
                 )
          })}
          </>
        )
      }
    }
    </Formik>
  )
}

export default GenericForm