import { ErrorMessage, Formik,Form } from 'formik';
import React from 'react'
import FormikControl from './components/formGroup/FormikControl';
import * as yup from 'yup';
import { regex } from './regexStore';
import { FormContainer } from './components/formGroup/Form.css';



const validationSchema  = yup.object().shape({
  full_name: yup.string().required("Full Name is required"),
  email:yup.string().required('Required').matches(new RegExp(regex.EMAIL),"Invalid Email"),
  password:yup.string().required("Please provide a valid password").matches(new RegExp(regex.PASSWORD),"Password Must be at least be 8characters and contain at least one numeric digit, one uppercase and one lowercase letter and a special character of !@#$%^&*"),
  cv:yup.mixed()
  .nullable()
  .notRequired()
  .test("FILE_SIZE", "Uploaded file is too big.", 
      value => !value || (value && value.size <= 500000))
  .test("FILE_FORMAT", "Uploaded file has unsupported format.", 
      value => !value || (value.includes(value.type))),
  hobbies:yup.string().required("Required"),
  interest:yup.string().required("Required"),
  contact:yup.string().required("Required").matches(new RegExp(regex.PHONENUMBER),"Phone must be of 10 digits"),
  gender:yup.string().required("Required"),
  date:yup.date().required("Required"),
  
});

const initialValues={
  full_name: "",
  email:"",
  password:"",
  cv:"",
  hobbies:"",
  interest:"",
  contact:"",
  gender:""
}


const Home = () => {
  return (
    <FormContainer>
    <Formik
       initialValues={initialValues}
       validationSchema={validationSchema}
       onSubmit={(values, { setSubmitting }) => {
         setTimeout(() => {
           alert(JSON.stringify(values, null, 2));
           setSubmitting(false);
         }, 400);
       }}
     >
       {({
         values,
         errors,
         touched,
         handleChange,
         handleBlur,
         handleSubmit,
         isSubmitting,
         /* and other goodies */
       }) => (
         <Form onSubmit={handleSubmit}>
           <FormikControl type="text" name="full_name" label="Full Name" />
           <FormikControl type="number" name="contact" label="Contact" />
           <FormikControl type="email" autoComplete={false} name="email" label="Email" />
           <FormikControl type="radio" name="gender" label="Gender" radioOptions={[{label:"Male",value:"Male"},{label:"Female",value:"Female"}]} />
           <FormikControl type="password" name="password" label="Password" />
           <FormikControl type="time" name="time" label="Time" />
           <FormikControl type="checkbox" label="Interest" name="interest" checkboxOptions={[{label:"Movies",value:"Movies"},{label:"Games",value:"Games"}]} />
           <FormikControl type="select" placeholder='Hobbies' name="hobbies" selectOptions={[{label:"Movies",value:"Movies"}]} />
           <FormikControl type="date" name="date" />
           <FormikControl type="file"  name="cv"/>
           <button type="submit" disabled={isSubmitting}>
             Submit
           </button>
         </Form>
       )}
     </Formik>
     </FormContainer>
  )
}

export default Home

